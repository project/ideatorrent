<?php


/**
 * Given a file containing a CCK export string, import them into the given content_type
 */
function ideatorrent_import_cck($filename, $content_type = '<create>')
{
	// Get the files content
	$content = implode ('', file ($filename));

	// Build form state
	$form_state = array(
		'values' => array(
			'type_name' => $content_type,
			'macro' => $content,
		),
	);

	// Put it in there
	drupal_execute("content_copy_import_form", $form_state);
}

/**
 * Add a taxonomy vocabulary to a content type.
 */
function ideatorrent_add_taxonomy_to_contenttype($voc_name, $contenttype)
{
	$vocs = taxonomy_get_vocabularies();
	foreach($vocs as $voc)
	{
		if($voc->name == $voc_name)
		{
			$voc->nodes[$contenttype] = $contenttype;
			taxonomy_save_vocabulary(get_object_vars($voc));
			return;
		}
	}
}

/**
 * Remove a taxonomy vocabulary to a content type.
 */
function ideatorrent_remove_taxonomy_from_contenttype($voc_name, $contenttype)
{
	$vocs = taxonomy_get_vocabularies();
	foreach($vocs as $voc)
	{
		if($voc->name == $voc_name)
		{
			unset($voc->nodes[$contenttype]);
			taxonomy_save_vocabulary(get_object_vars($voc));
			return;
		}
	}
}


/**
 * Add a role, if not already present
 */
function _ideatorrent_add_role($role_name)
{
  require_once(drupal_get_path('module', 'user') . "/user.admin.inc");

  //Fetch the current roles, see if it already exists
  $already_present = false;
  foreach(user_roles() as $id => $name)
  {
    if($name == $role_name)
      $already_present = true;
  }

  if($already_present == false)
  {
    $form_id = "user_admin_new_role";
    $form_values = array();
    $form_values["name"] = $role_name;
    $form_values["op"] = t('Add role');
    $form_state = array();
    $form_state["values"] = $form_values;

    drupal_execute($form_id, $form_state);
  }
}


/**
 * Get a IdeaTorrent role ID.
 */
define('IDEATORRENT_ROLE_USER', 1);
define('IDEATORRENT_ROLE_MODERATOR', 2);
define('IDEATORRENT_ROLE_MANAGER', 3);
define('IDEATORRENT_ROLE_SUPER_ADMIN', 4);
function ideatorrent_get_role_id($ideatorrent_role)
{
  static $mapping = array();

  if(empty($mapping))
  {
    $roles = user_roles();
    foreach($roles as $id => $role_name)
    {
      if($role_name == t('IdeaTorrent user'))
        $mapping[IDEATORRENT_ROLE_USER] = $id;
      else if($role_name == t('IdeaTorrent moderator'))
        $mapping[IDEATORRENT_ROLE_MODERATOR] = $id;
      else if($role_name == t('IdeaTorrent manager'))
        $mapping[IDEATORRENT_ROLE_MANAGER] = $id;
      else if($role_name == t('IdeaTorrent super admin'))
        $mapping[IDEATORRENT_ROLE_SUPER_ADMIN] = $id;
    }
  }

  return $mapping[$ideatorrent_role];
}

/**
 * Install the IdeaTorrent Workflow settings
 */
function _ideatorrent_install_workflow()
{
  //Add the IdeaTorrent workflow
  $ideatorrent_workflow_name = "IdeaTorrent Workflow";
  $wid = 0;
  foreach(workflow_get_all() as $id => $name)
  {
    if($name == $ideatorrent_workflow_name)
      $wid = $id;
  }
  if($wid == 0)
  {
    $wid = workflow_create($ideatorrent_workflow_name);
    $sid_awaiting_moderation = workflow_state_save(array(
      'wid' => $wid,
      'state' => t('Awaiting moderation'),
      'weight' => 1));
    $sid_new = workflow_state_save(array(
      'wid' => $wid,
      'state' => t('New'),
      'weight' => 2));
    $sid_in_development = workflow_state_save(array(
      'wid' => $wid,
      'state' => t('In development'),
      'weight' => 3));
    $sid_implemented = workflow_state_save(array(
      'wid' => $wid,
      'state' => t('Implemented'),
      'weight' => 4));
    $sid_invalid = workflow_state_save(array(
      'wid' => $wid,
      'state' => t('Invalid'),
      'weight' => 5));
    $sid_duplicate = workflow_state_save(array(
      'wid' => $wid,
      'state' => t('Duplicate'),
      'weight' => 6));
    $sid_wont_implement = workflow_state_save(array(
      'wid' => $wid,
      'state' => t("Won't implement"),
      'weight' => 7));

    //Configure the workflow
    $all_states = workflow_get_states($wid);
    $all_states_minus_creation = $all_states;
    $creation_state = array_pop($all_states_minus_creation);
    foreach($all_states as $start_state => $start_state_name)
    {
      foreach($all_states_minus_creation as $end_state => $end_state_name)
      {
        if($start_state != $end_state)
        {
          workflow_transition_add_role($start_state, $end_state, ideatorrent_get_role_id(IDEATORRENT_ROLE_SUPER_ADMIN));
          workflow_transition_add_role($start_state, $end_state, ideatorrent_get_role_id(IDEATORRENT_ROLE_MANAGER));
        }
      }
    }
    $moderator_states = array($sid_awaiting_moderation, $sid_new, $sid_invalid, $sid_duplicate);
    $moderator_states = array($sid_awaiting_moderation, $sid_new, $sid_invalid, $sid_duplicate);
    foreach($moderator_states as $start_state)
    {
      foreach($moderator_states as $end_state)
      {
        if($start_state != $end_state)
        {
          workflow_transition_add_role($start_state, $end_state, ideatorrent_get_role_id(IDEATORRENT_ROLE_MODERATOR));
        }
      }
    }

    //Attach this transition to the ideatorrent_idea and ideatorrent_rationale content type
    module_load_include('inc', 'workflow', 'workflow.admin');
    $form_state = array(
      "values" => array(
        'ideatorrent_idea' => array(
          'workflow' => $wid, 
          'placement' => array(
            "node" => true,
            "comment" => false,
          ),
        ),
        'ideatorrent_rationale' => array(
          'workflow' => $wid, 
          'placement' => array(
            "node" => true,
            "comment" => false,
          ),
        ),
      ),
    );
    drupal_execute('workflow_types_form', $form_state);

    //Set the node view/edition/deletion permission according to workflow states
    $workflow = workflow_load($wid);
    $form_state = array("values" => array("wid" => $wid, "wf_name" => $workflow->name));

    foreach($moderator_states as $state_id)
    {
      $form_state['values']['workflow_access'][$state_id]['update'][ideatorrent_get_role_id(IDEATORRENT_ROLE_MODERATOR)] = true;
      //Author
      $form_state['values']['workflow_access'][$state_id]['update'][-1] = true;
    }
    foreach($all_states as $state_id => $state_name)
    {
      $form_state['values']['workflow_access'][$state_id]['view'][ideatorrent_get_role_id(IDEATORRENT_ROLE_MANAGER)] = true;
      $form_state['values']['workflow_access'][$state_id]['view'][ideatorrent_get_role_id(IDEATORRENT_ROLE_SUPER_ADMIN)] = true;
      $form_state['values']['workflow_access'][$state_id]['view'][ideatorrent_get_role_id(IDEATORRENT_ROLE_MODERATOR)] = true;
      $form_state['values']['workflow_access'][$state_id]['view'][ideatorrent_get_role_id(IDEATORRENT_ROLE_USER)] = true;

      $form_state['values']['workflow_access'][$state_id]['update'][ideatorrent_get_role_id(IDEATORRENT_ROLE_MANAGER)] = true;
      $form_state['values']['workflow_access'][$state_id]['update'][ideatorrent_get_role_id(IDEATORRENT_ROLE_SUPER_ADMIN)] = true;
    }
    //Not really proud of it... but well...
    workflow_access_form_submit(drupal_retrieve_form('workflow_edit_form', $form_state, $workflow), $form_state);

  }

}

/**
 * Return an array containing the current filter of the given path (if null, the current path)
 */
function _ideatorrent_get_current_filters($path = null)
{
  $result = array(
    "category" => null,
    "workflow" => null,
    "ordering" => null,
    "milestone" => null,
  );

  if($path == null)
    $path = $_GET['q'];
  if($path == "")
    $path = variable_get('site_frontpage', 'node');

  $parts = explode("/", $path);

  if($parts[0] == "ideatorrent")
  {
    $result["category"] = ($parts[1] != "all") ? $parts[1] : null;
    $result["workflow"] = ($parts[2] != "all") ? $parts[2] : null;
    $result["ordering"] = $parts[3];
    $result["milestone"] = ($parts[4] != "all") ? $parts[4] : null;
  }

  return $result;
}

/**
 * Install the ideatorrent menu
 */
/**function _ideatorrent_install_menu()
{
  module_load_include('inc', 'menu', 'menu.admin');
  $form_state = array(
    'values' => array(
      'menu_name' => 'ideatorrent',
      'title' => 'IdeaTorrent',
      'description' => 'The main IdeaTorrent menu.',
    ),
  );
  drupal_execute('menu_edit_menu', $form_state, '');


  $item = array(
    "menu_name" => "ideatorrent",
    "link_path" => "ideatorrent/all/3/most_popular_this_month",
    "link_title" => "Popular ideas",
  );
  menu_link_save($item);
}*/
