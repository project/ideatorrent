<?php

define('IDEATORRENT_BLOCK_CATEGORY', 0);
define('IDEATORRENT_BLOCK_WORKFLOW', 1);
define('IDEATORRENT_BLOCK_ORDERING', 2);
define('IDEATORRENT_BLOCK_MILESTONE', 3);

/**
 * Implementation of hook_block().
 */
function ideatorrent_block($op = 'list', $delta = 0, $edit = array()) {
  $multiblock_id = ($edit['multiblock_delta']['#value'] != "") ? $edit['multiblock_delta']['#value'] : 0;

  switch ($op) {
    case 'list':
      $blocks[IDEATORRENT_BLOCK_CATEGORY]['info'] = 'IdeaTorrent category list';
      $blocks[IDEATORRENT_BLOCK_WORKFLOW]['info'] = 'IdeaTorrent workflow list';
      $blocks[IDEATORRENT_BLOCK_ORDERING]['info'] = 'IdeaTorrent ordering list';
      $blocks[IDEATORRENT_BLOCK_MILESTONE]['info'] = 'IdeaTorrent milestone list';
      return $blocks;

    case 'configure':

      if ($delta == IDEATORRENT_BLOCK_CATEGORY) {
      }
    return $form;

    case 'save':
      if ($delta == IDEATORRENT_BLOCK_CATEGORY) {
      }
    return;

    case 'view':
      if ($delta == IDEATORRENT_BLOCK_CATEGORY) 
      {
        $block['subject'] = '';
        $block['content'] = ideatorrent_block_category();
      }
      else if ($delta == IDEATORRENT_BLOCK_WORKFLOW) 
      {
        $block['subject'] = '';
        $block['content'] = ideatorrent_block_workflow();
      }
      else if ($delta == IDEATORRENT_BLOCK_ORDERING) 
      {
        $block['subject'] = '';
        $block['content'] = ideatorrent_block_ordering();
      }
      else if ($delta == IDEATORRENT_BLOCK_MILESTONE) 
      {
        $block['subject'] = '';
        $block['content'] = ideatorrent_block_milestone();
      }
      return $block;

    case 'mb_enabled':
      if($delta == 0) {
        return 'mb_enabled';
      }
    break;
  }
}


/**
 * Render the block category.
 */
function ideatorrent_block_category()
{
  $result = "";
  $paths = array();
  $terms = array();
  $filters = _ideatorrent_get_current_filters();

	$vocs = taxonomy_get_vocabularies();
	foreach($vocs as $voc)
	{
		if($voc->name == "IdeaTorrent categories")
		{
      $terms = taxonomy_get_tree($voc->vid);
      break;
		}
	}

  foreach($terms as $term)
  {
    $paths[$term->name] = "ideatorrent/" . $term->name . "/" . 
      (($filters['workflow'] != null)? $filters['workflow'] : "all") . "/" . 
      (($filters['ordering'] != null)? $filters['ordering'] : "most_popular_this_month") . "/" . 
      (($filters['release'] != null)? $filters['release'] : "all");
  }

  $result .= theme('ideatorrent_block_category', $paths);

  return $result;
}

/**
 * Render the block workflow.
 */
function ideatorrent_block_workflow()
{
  $result = "";
  $paths = array();
  $filters = _ideatorrent_get_current_filters();


  $paths[t("Idea sandbox")] = "ideatorrent/" . 
    (($filters['category'] != null)? $filters['category'] : "all") . "/" . 
    "1+2+6+7/" . 
    (($filters['ordering'] != null && ($filters['ordering'] == "latest_comments" || $filters['ordering'] == "latest_ideas"))? $filters['ordering'] : "latest_ideas") . "/" . 
    "all";

  $paths[t("Popular ideas")] = "ideatorrent/" . 
    (($filters['category'] != null)? $filters['category'] : "all") . "/" . 
    "3/" . 
    (($filters['ordering'] != null)? $filters['ordering'] : "most_popular_this_month") . "/" . 
    "all";

  $paths[t("Ideas in development")] = "ideatorrent/" . 
    (($filters['category'] != null)? $filters['category'] : "all") . "/" . 
    "4/" . 
    (($filters['ordering'] != null && ($filters['ordering'] == "latest_comments" || $filters['ordering'] == "latest_ideas"))? $filters['ordering'] : "latest_ideas") . "/" . 
    "all";

  $paths[t("Implemented ideas")] = "ideatorrent/" . 
    (($filters['category'] != null)? $filters['category'] : "all") . "/" . 
    "5/" . 
    (($filters['ordering'] != null && ($filters['ordering'] == "latest_comments" || $filters['ordering'] == "latest_ideas"))? $filters['ordering'] : "latest_ideas") . "/" . 
    "all";

  $result .= theme('ideatorrent_block_workflow', $paths);

  return $result;
}

/**
 * Render the block ordering.
 */
function ideatorrent_block_ordering()
{
  $result = "";
  $paths = array();
  $filters = _ideatorrent_get_current_filters();

  $orderings = array(
    t("Latest") => "latest_ideas",
    t("Most popular in 24H") => "most_popular_today",
    t("Most popular this week") => "most_popular_this_week",
    t("Most popular in 30 days") => "most_popular_this_month",
    t("Most popular in 6 months") => "most_popular_6_months",
    t("Most popular ever") => "most_popular_ever",
    t("Latest comments") => "latest_comments",
    t("Random") => "random_ideas",
    );

  foreach($orderings as $ordering_name => $ordering_path)
  {
    $paths[$ordering_name] = "ideatorrent/" .
      (($filters['category'] != null)? $filters['category'] : "all") . "/" . 
      (($filters['workflow'] != null)? $filters['workflow'] : "all") . "/" . 
      $ordering_path . "/" . 
      (($filters['release'] != null)? $filters['release'] : "all");
  }

  $result .= theme('ideatorrent_block_ordering', $paths);

  return $result;
}

/**
 * Render the block milestone.
 */
function ideatorrent_block_milestone()
{
  $result = "";
  $paths = array();
  $terms = array();
  $filters = _ideatorrent_get_current_filters();

	$vocs = taxonomy_get_vocabularies();
	foreach($vocs as $voc)
	{
		if($voc->name == "Idea Milestone")
		{
      $terms = taxonomy_get_tree($voc->vid);
      break;
		}
	}

  if($filters['workflow'] == "4" || $filters['workflow'] == "5")
  {
    foreach($terms as $term)
    {
      $paths[$term->name] = "ideatorrent/" . 
        (($filters['category'] != null)? $filters['category'] : "all") . "/" .  
        (($filters['workflow'] != null)? $filters['workflow'] : "all") . "/" . 
        (($filters['ordering'] != null)? $filters['ordering'] : "most_popular_this_month") . "/" . 
        $term->name;
    }
  }

  $result .= theme('ideatorrent_block_milestone', $paths);

  return $result;
}
