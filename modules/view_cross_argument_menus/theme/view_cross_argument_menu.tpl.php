<?php
?>

<div class="view-cross-argument-menu">

  <?php foreach($menu_entries as $menu_name => $menu_url) : ?>

  <div class="view-cross-argument-menu-item">

    <a href="<?php echo url($menu_url); ?>"><?php echo check_plain($menu_name); ?></a>

  </div>

  <?php endforeach; ?>

</div>


