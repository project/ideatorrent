<?php


/**
 * @file
 * IdeaTorrent vote formatter hooks and callbacks.
 */

function theme_ideatorrent_vote_formatter_default($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

  drupal_add_css(drupal_get_path('module', 'ideatorrent_vote') . '/theme/ideatorrent_vote.css');
  drupal_add_js(drupal_get_path('module', 'ideatorrent_vote') . '/ideatorrent_vote.js');

  $html = theme('ideatorrent_vote_plus_minus_zero_proportion',
    $element['#node']->nid,
    $element['#field_name'],
    (empty($element['#item']['votesum']) == false)?$element['#item']['votesum']:0,
    (empty($element['#item']['votecount']) == false)?$element['#item']['votecount']:0,
    (empty($element['#item']['votecountplus']) == false)?$element['#item']['votecountplus']:0,
    (empty($element['#item']['votecountminus']) == false)?$element['#item']['votecountminus']:0,
    (empty($element['#item']['votesumequal']) == false)?$element['#item']['votesumequal']:0,
    (empty($element['#item']['votepopularityday']) == false)?$element['#item']['votepopularityday']:0,
    (empty($element['#item']['votepopularity']) == false)?$element['#item']['votepopularity']:0
    );

	return $html;
}


function theme_ideatorrent_vote_formatter_plus_minus_zero($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

  drupal_add_css(drupal_get_path('module', 'ideatorrent_vote') . '/theme/ideatorrent_vote.css');
  drupal_add_js(drupal_get_path('module', 'ideatorrent_vote') . '/ideatorrent_vote.js');

  $html = theme('ideatorrent_vote_plus_minus_zero',
    $element['#node']->nid,
    $element['#field_name'],
    (empty($element['#item']['votesum']) == false)?$element['#item']['votesum']:0,
    (empty($element['#item']['votecount']) == false)?$element['#item']['votecount']:0,
    (empty($element['#item']['votecountplus']) == false)?$element['#item']['votecountplus']:0,
    (empty($element['#item']['votecountminus']) == false)?$element['#item']['votecountminus']:0,
    (empty($element['#item']['votesumequal']) == false)?$element['#item']['votesumequal']:0,
    (empty($element['#item']['votepopularityday']) == false)?$element['#item']['votepopularityday']:0,
    (empty($element['#item']['votepopularity']) == false)?$element['#item']['votepopularity']:0
    );

	return $html;
}


function theme_ideatorrent_vote_formatter_plus_minus_proportion($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

  drupal_add_css(drupal_get_path('module', 'ideatorrent_vote') . '/theme/ideatorrent_vote.css');
  drupal_add_js(drupal_get_path('module', 'ideatorrent_vote') . '/ideatorrent_vote.js');

  $html = theme('ideatorrent_vote_plus_minus_proportion',
    $element['#node']->nid,
    $element['#field_name'],
    (empty($element['#item']['votesum']) == false)?$element['#item']['votesum']:0,
    (empty($element['#item']['votecount']) == false)?$element['#item']['votecount']:0,
    (empty($element['#item']['votecountplus']) == false)?$element['#item']['votecountplus']:0,
    (empty($element['#item']['votecountminus']) == false)?$element['#item']['votecountminus']:0,
    (empty($element['#item']['votesumequal']) == false)?$element['#item']['votesumequal']:0,
    (empty($element['#item']['votepopularityday']) == false)?$element['#item']['votepopularityday']:0,
    (empty($element['#item']['votepopularity']) == false)?$element['#item']['votepopularity']:0
    );

	return $html;
}


function theme_ideatorrent_vote_formatter_plus_minus($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

  drupal_add_css(drupal_get_path('module', 'ideatorrent_vote') . '/theme/ideatorrent_vote.css');
  drupal_add_js(drupal_get_path('module', 'ideatorrent_vote') . '/ideatorrent_vote.js');

  $html = theme('ideatorrent_vote_plus_minus',
    $element['#node']->nid,
    $element['#field_name'],
    (empty($element['#item']['votesum']) == false)?$element['#item']['votesum']:0,
    (empty($element['#item']['votecount']) == false)?$element['#item']['votecount']:0,
    (empty($element['#item']['votecountplus']) == false)?$element['#item']['votecountplus']:0,
    (empty($element['#item']['votecountminus']) == false)?$element['#item']['votecountminus']:0,
    (empty($element['#item']['votesumequal']) == false)?$element['#item']['votesumequal']:0,
    (empty($element['#item']['votepopularityday']) == false)?$element['#item']['votepopularityday']:0,
    (empty($element['#item']['votepopularity']) == false)?$element['#item']['votepopularity']:0
    );

	return $html;
}


function theme_ideatorrent_vote_formatter_plus($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

  drupal_add_css(drupal_get_path('module', 'ideatorrent_vote') . '/theme/ideatorrent_vote.css');
  drupal_add_js(drupal_get_path('module', 'ideatorrent_vote') . '/ideatorrent_vote.js');

  $html = theme('ideatorrent_vote_plus',
    $element['#node']->nid,
    $element['#field_name'],
    (empty($element['#item']['votesum']) == false)?$element['#item']['votesum']:0,
    (empty($element['#item']['votecount']) == false)?$element['#item']['votecount']:0,
    (empty($element['#item']['votecountplus']) == false)?$element['#item']['votecountplus']:0,
    (empty($element['#item']['votecountminus']) == false)?$element['#item']['votecountminus']:0,
    (empty($element['#item']['votesumequal']) == false)?$element['#item']['votesumequal']:0,
    (empty($element['#item']['votepopularityday']) == false)?$element['#item']['votepopularityday']:0,
    (empty($element['#item']['votepopularity']) == false)?$element['#item']['votepopularity']:0
    );

	return $html;
}
