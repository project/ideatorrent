<?php


/**
 * Create/Read/Update/Delete operations
 */


/**
 * Add or update a vote
 */
function ideatorrent_vote_add_or_create($nid, $field_name, $value, $source_type, $source_value)
{
  $criterias = array();
  if($nid != null)
    $criterias['nid'] = $nid;
  if($field_name != null)
    $criterias['cck_field_id'] = _ideatorrent_vote_get_or_create_field_id($field_name);
  if(is_really_integer($source_type) && $source_value != null)
    $criterias['vote_source'] = _ideatorrent_vote_get_or_create_vote_source($source_type, $source_value);

  $votes = ideatorrent_vote_read($criterias);
  if(count($votes) == 0)
  {
    ideatorrent_vote_add($nid, $field_name, $value, $source_type, $source_value);
  }
  else if(count($votes) == 1)
  {
    ideatorrent_vote_update($votes[0], $value);
  }
  else
  {
    //We shouldn't be here... Do nothing
  }
}

/**
 * Add a vote
 */
function ideatorrent_vote_add($nid, $field_name, $value, $source_type, $source_value)
{
  if(($node = node_load($nid)) && $field_name != "" && is_really_integer($value) && is_really_integer($source_type) && $source_value != "")
  {
    $new_vote['nid'] = $nid;
    $new_vote['vote_source'] = _ideatorrent_vote_get_or_create_vote_source($source_type, $source_value);
    $new_vote['cck_field_id'] = _ideatorrent_vote_get_or_create_field_id($field_name);
    $new_vote['value'] = $value;
    $new_vote['timestamp'] = time();
    drupal_write_record('ideatorrent_vote_vote', $new_vote);
    //update the stats
    _ideatorrent_vote_update_stats($node, $field_name, ($value == 1)?1:0, ($value == 0)?1:0, ($value == -1)?1:0);
  }
}

/**
 * Read vote(s).
 */
function ideatorrent_vote_read($criterias)
{
  $vote_schema = drupal_get_schema('ideatorrent_vote_vote');
  $field_schema = drupal_get_schema('ideatorrent_vote_field');
  $source_schema = drupal_get_schema('ideatorrent_vote_source');
  $votes = $conditions = $values = array();
  foreach ($criterias as $criteria => $value){
    if($criteria == "field_id" || $criteria == "field_name")
    {
      $schema = $field_schema;
    }
    else if($criteria == "source_id" || $criteria == "source_type" || 
      $criteria == "source_value_numeric" || $criteria == "source_value_text")
    {
      $schema = $source_schema;
    }
    else
      $schema = $vote_schema;

    $conditions[] = "$criteria = ". db_type_placeholder($schema['fields'][$criteria]['type']);
    $values[] = $value;
  }

  $results = db_query("SELECT vote_id, nid, cck_field_id, vote.vote_source, value, timestamp, field_name,
    source_type, source_value_numeric, source_value_text
    FROM {ideatorrent_vote_vote} vote
    LEFT JOIN {ideatorrent_vote_field} field ON field.field_id = vote.cck_field_id
    LEFT JOIN {ideatorrent_vote_source} source ON source.source_id = vote.vote_source
    WHERE " . implode(' AND ', $conditions), $values);

  while($vote = db_fetch_array($results))
    $votes[] = $vote;

  return $votes;
}

/**
 * Update an existing vote.
 * @param vote The vote array fetched from database.
 */
function ideatorrent_vote_update($vote, $new_value)
{
  $old_value = $vote['value'];

  $vote_update['vote_id'] = $vote['vote_id'];
  $vote_update['value'] = $new_value;
  drupal_write_record('ideatorrent_vote_vote', $vote_update, 'vote_id');

  //Update the global stats
  $plus_delta = (($old_value == 1)?-1:0) + (($new_value == 1)?1:0);
  $equal_delta = (($old_value == 0)?-1:0) + (($new_value == 0)?1:0);
  $minus_delta = (($old_value == -1)?-1:0) + (($new_value == -1)?1:0);
  _ideatorrent_vote_update_stats(node_load($vote['nid']), $vote['field_name'], $plus_delta, $equal_delta, $minus_delta);
}

/**
 * Get (and optionally create) a vote source, return its id.
 */
function _ideatorrent_vote_get_or_create_vote_source($type, $value)
{
  if(is_really_integer($value))
  {
    $result = db_result(db_query("SELECT source_id FROM {ideatorrent_vote_source} WHERE source_type = %d AND source_value_numeric = %d", $type, $value));
  }
  else
  {
    $result = db_result(db_query("SELECT source_id FROM {ideatorrent_vote_source} WHERE source_type = %d AND source_value_text = '%s'", $type, $value));
  }

  if(is_numeric($result) == false || $result <= 0)
  {
    //Not found. Create it.
    $new_source['source_type'] = $type;
    if(is_really_integer($value))
    {
      $new_source['source_value_numeric'] = $value;
    }
    else
    {
      $new_source['source_value_text'] = $value;      
    }
    drupal_write_record('ideatorrent_vote_source', $new_source);
    $result = $new_source['source_id'];
  }

  return $result;
}

/**
 * Get (and optionally create) a field id.
 */
function _ideatorrent_vote_get_or_create_field_id($field_name)
{
  $result = db_result(db_query("SELECT field_id FROM {ideatorrent_vote_field} WHERE field_name = '%s'", $field_name));

  if(is_numeric($result) == false || $result <= 0)
  {
    //Not found. Create it.
    $new_source['field_name'] = $field_name;
    drupal_write_record('ideatorrent_vote_field', $new_source);
    $result = $new_source['field_id'];
  }

  return $result;
}

/**
 * Update the CCK field stats.
 * Give as parameter what happened (e.g. a plus vote was removed, a minus vote was added instead)
 */
function _ideatorrent_vote_update_stats($node, $field_name, $plus_delta, $equal_delta, $minus_delta)
{
  if($node != null)
  {
    $node->{$field_name}[0]['votesum'] += ($plus_delta * 1 + $equal_delta * 0 + $minus_delta * (-1));
    $node->{$field_name}[0]['votecount'] += ($plus_delta + $equal_delta + $minus_delta);
    $node->{$field_name}[0]['votecountplus'] += $plus_delta;
    $node->{$field_name}[0]['votecountequal'] += $equal_delta;
    $node->{$field_name}[0]['votecountminus'] += $minus_delta;

    //Save our changes
    node_save($node);

    //Update the linked vote field.
    //We will look for the nodes using the selected field in the vote field config that reference this node
    //and update its vote stats. => It will contains the stats of the highest score node.
    $fields = content_fields();
    $vote_field_infos = $fields[$field_name];
    if($vote_field_infos['report_vote'] != "" && $vote_field_infos['report_vote_vote_field'] != "")
    {
      if($noderef_field_infos = $fields[$vote_field_infos['report_vote']])
      {
        //Look for the nodes that reference our nodes using this field
        module_load_include('inc', 'views', 'includes/view');
        $view = new view;
        $view->base_table = 'node';
        $handler = $view->new_display('default', 'Defaults', 'default');
        $handler->override_option('fields', array(
          'nid' => array(
            'id' => 'nid',
            'table' => 'node',
            'field' => 'nid',
            'relationship' => 'none',
          ),
        ));
        $handler->override_option('filters', array(
          $vote_field_infos['report_vote'] . '_nid' => array(
            'operator' => 'or',
            'value' => array(
              $node->nid => $node->nid,
            ),
            'id' => $vote_field_infos['report_vote'] . '_nid',
            'table' => 'node_data_' . $vote_field_infos['report_vote'],
            'field' => $vote_field_infos['report_vote'] . '_nid',
            'reduce_duplicates' => 1,
          ),
        ));
        $view->init_display();
        $view->display_handler->set_option('items_per_page', 1000);
        $view->pre_execute();
        $view->execute();

        //For each of these nodes, look at all the node linked back, and choose the one with the highest score.
        //E.g. if our current node A point to a node B, and this node B points back to our node A and another one C, let's put
        //in B the best score of these two nodes A and C.
        foreach($view->result as $delta => $result)
        {
          $linked_vote_node = node_load($result->nid);
          $best_score = 0;
          $best_node = null;
          foreach($linked_vote_node->{$vote_field_infos['report_vote']} as $delta2 => $back_node_nid)
          {
            $back_node = node_load($back_node_nid['nid'], null, true);
            if($best_node == null || $best_score < $back_node->{$field_name}[0]['votesum'])
            {
              $best_node = $back_node;
              $best_score = $back_node->{$field_name}[0]['votesum'];
            }
          }
          //Okay, we got the node with the best score now. Copy its stats (except the vote velocities) to the node B, and save it.
          unset($best_node->{$field_name}[0]['votepopularityday']);
          unset($best_node->{$field_name}[0]['votepopularity']);
          $linked_vote_node_vote_values = 
            (is_array($linked_vote_node->{$vote_field_infos['report_vote_vote_field']}[0])) ? 
              $linked_vote_node->{$vote_field_infos['report_vote_vote_field']}[0] : array();
          $linked_vote_node->{$vote_field_infos['report_vote_vote_field']}[0] = 
            $best_node->{$field_name}[0] + $linked_vote_node_vote_values;
          node_save($linked_vote_node);

        }


      }
    }
  }
}

/**
 * Checks if the given value represents integer
 */
function is_really_integer($val)
{
    return ($val !== true) && ((string)(int) $val) === ((string) $val);
}
