<?php


/**
 * Implementation of hook_field_info().
 */
function ideatorrent_vote_field_info() {
  return array(
    'vote' => array(
      'label' => t('Vote'),
      'description' => t('Store votes in the database.'),
    ),
  );
}


/**
 * Implementation of hook_field_settings().
 */
function ideatorrent_vote_field_settings($op, $field) {
  switch ($op) {
    case 'form':
      $form = array();
      $form['allow_anonymous_voting'] = array(
        '#type' => 'checkbox',
        '#title' => t('Allow anonymous voting'),
        '#default_value' => !empty($field['allow_anonymous_voting']) ? $field['allow_anonymous_voting'] : true,
      );
      $form['report_vote'] = array(
        '#type' => 'select',
        '#title' => t('Report votes to another node'),
        '#description' => t('INTERNAL. USE IF YOU KNOW WHAT YOU ARE DOING. Nodes which reference the current node with the selected field will have their vote stats updated. Used to order rationale/solutions ideas by vote.'),
        '#options' => _ideatorrent_vote_nodereference_options(),
        '#default_value' => !empty($field['report_vote']) ? $field['report_vote']  : '',
      );
      $form['report_vote_vote_field'] = array(
        '#type' => 'select',
        '#title' => t('Report votes to another node, vote field'),
        '#description' => t('INTERNAL. USE IF YOU KNOW WHAT YOU ARE DOING. The vote field where the vote stats will be updated.'),
        '#options' => _ideatorrent_vote_vote_options(),
        '#default_value' => !empty($field['report_vote_vote_field']) ? $field['report_vote_vote_field']  : '',
      );
      return $form;

    case 'save':
      return array('allow_anonymous_voting', 'report_vote', 'report_vote_vote_field');

    case 'database columns':
      $columns['votesum'] = array('type' => 'int', 'unsigned' => FALSE, 'not null' => FALSE, 'views' => TRUE, 'default' => 0);
      $columns['votecount'] = array('type' => 'int', 'unsigned' => FALSE, 'not null' => FALSE, 'views' => TRUE, 'default' => 0);
      $columns['votecountplus'] = array('type' => 'int', 'unsigned' => FALSE, 'not null' => FALSE, 'views' => TRUE, 'default' => 0);
      $columns['votecountminus'] = array('type' => 'int', 'unsigned' => FALSE, 'not null' => FALSE, 'views' => TRUE, 'default' => 0);
      $columns['votecountequal'] = array('type' => 'int', 'unsigned' => FALSE, 'not null' => FALSE, 'views' => TRUE, 'default' => 0);
      $columns['votepopularityday'] = array('type' => 'float', 'unsigned' => FALSE, 'not null' => FALSE, 'views' => TRUE, 'default' => 0);
      $columns['votepopularity'] = array('type' => 'float', 'unsigned' => FALSE, 'not null' => FALSE, 'views' => TRUE, 'default' => 0);
      return $columns;

    case 'views data':
      $data = content_views_field_views_data($field);
      $db_info = content_database_info($field);
      $table_alias = content_views_tablename($field);

      $data[$table_alias][$field['field_name'] .'_votesum']['sort']['handler'] = 'views_handler_sort';
      $data[$table_alias][$field['field_name'] .'_votepopularityday']['sort']['handler'] = 'views_handler_sort';
      $data[$table_alias][$field['field_name'] .'_votepopularity']['sort']['handler'] = 'views_handler_sort';

      return $data;
  }
}


/**
 * Implementation of hook_field().
 */
function ideatorrent_vote_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
//    case 'validate':
//      return $items;

//    case 'sanitize':
  }
}

/**
 * Implementation of hook_content_is_empty().
 */
function ideatorrent_vote_content_is_empty($item, $field) {
  return FALSE;
}

/**
 * Implementation of hook_field_formatter_info().
 */
function ideatorrent_vote_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t('+1/0/-1 vote widget plus proportion bar'),
      'field types' => array('vote'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'plus_minus_zero' => array(
      'label' => t('+1/0/-1 vote widget'),
      'field types' => array('vote'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'plus_minus_proportion' => array(
      'label' => t('+1/-1 vote widget plus proportion bar'),
      'field types' => array('vote'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'plus_minus' => array(
      'label' => t('+1/-1 vote widget'),
      'field types' => array('vote'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'plus' => array(
      'label' => t('+1 vote widget'),
      'field types' => array('vote'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}


/**
 * Implementation of hook_widget_info().
 *
 * Here we indicate that the content module will handle
 * the default value and multiple values for these widgets.
 *
 * Callbacks can be omitted if default handing is used.
 * They're included here just so this module can be used
 * as an example for custom modules that might do things
 * differently.
 */
function ideatorrent_vote_widget_info() {
  return array(
    'ideatorrent_vote_edit' => array(
      'label' => t('No edit widget'),
      'field types' => array('vote'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array(
        'default value' => CONTENT_CALLBACK_DEFAULT,
      ),
    ),
  );
}

/**
 * Implementation of hook_widget_settings().
 */
function ideatorrent_vote_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();
      return $form;

    case 'save':
      return array();
  }
}


/**
 * Implementation of hook_widget().
 *
 * Attach a single form element to the form. It will be built out and
 * validated in the callback(s) listed in hook_elements. We build it
 * out in the callbacks rather than here in hook_widget so it can be
 * plugged into any module that can provide it with valid
 * $field information.
 *
 * Content module will set the weight, field name and delta values
 * for each form element. This is a change from earlier CCK versions
 * where the widget managed its own multiple values.
 *
 * If there are multiple values for this field, the content module will
 * call this function as many times as needed.
 *
 * @param $form
 *   the entire form array, $form['#node'] holds node information
 * @param $form_state
 *   the form_state, $form_state['values'][$field['field_name']]
 *   holds the field's form values.
 * @param $field
 *   the field array
 * @param $items
 *   array of default values for this field
 * @param $delta
 *   the order of this item in the array of subelements (0, 1, 2, etc)
 *
 * @return
 *   the form item for a single element for this field
 */
function ideatorrent_vote_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array();
  return $element;
}


/**
 * Fetch all the nodereference fields
 */
function _ideatorrent_vote_nodereference_options()
{
  $fields = content_fields();

  $noderef_options[''] = '- None -';
  foreach ($fields as $field_id => $field_info)
  {
    if($field_info['type'] == 'nodereference')
      $noderef_options[$field_id] = $field_id;
  }

  return $noderef_options;
}

/**
 * Fetch all the vote fields
 */
function _ideatorrent_vote_vote_options()
{
  $fields = content_fields();

  $noderef_options[''] = '- None -';
  foreach ($fields as $field_id => $field_info)
  {
    if($field_info['type'] == 'vote')
      $noderef_options[$field_id] = $field_id;
  }

  return $noderef_options;
}
