
Drupal.behaviors.ideatorrentVoteWidget = function(context)
{

  $('.ideatorrent-votewidget').each(function(){

    var widget = $(this);
    var elements = widget.attr('id').split(new RegExp("[-]+", "g"));

    var field_name = elements[elements.length - 1];
    var nid = elements[elements.length - 2];

    //Get the ids of the differents elements
    var linkToNolink = new Array();
    var pluslink = '#ideatorrent-votewidget-votearea-plus-link-' + nid + '-' + field_name;
    var plusnolink = '#ideatorrent-votewidget-votearea-plus-nolink-' + nid + '-' + field_name;
    linkToNolink[pluslink] = plusnolink;
    var equallink = '#ideatorrent-votewidget-votearea-equal-link-' + nid + '-' + field_name;
    var equalnolink = '#ideatorrent-votewidget-votearea-equal-nolink-' + nid + '-' + field_name;
    linkToNolink[equallink] = equalnolink;
    var minuslink = '#ideatorrent-votewidget-votearea-minus-link-' + nid + '-' + field_name;
    var minusnolink = '#ideatorrent-votewidget-votearea-minus-nolink-' + nid + '-' + field_name;
    linkToNolink[minuslink] = minusnolink;

    //Get the tokens
    var tokens = new Array();
    var linkparts = $(pluslink).attr('href').split(new RegExp("token=", "g"));
    tokens[pluslink] = linkparts[linkparts.length - 1];
    var linkparts = $(equallink).attr('href').split(new RegExp("token=", "g"));
    tokens[equallink] = linkparts[linkparts.length - 1];
    var linkparts = $(minuslink).attr('href').split(new RegExp("token=", "g"));
    tokens[minuslink] = linkparts[linkparts.length - 1];

    //Get the current score
    var scoreArea = $('#ideatorrent-votewidget-votesum-' + nid + '-' + field_name);


    //Set the behavior when clicking on one of the arrow
    $(pluslink).add(equallink).add(minuslink).click(function() {

      //Get the current value in the UI, update the UI
      var currentScore = parseInt(scoreArea.text());
      var currentVote = 0;
      var newVote = 0;
      if($(pluslink).hasClass('ideatorrent-votewidget-link-unvoted') == false)
      {
        if($(minusnolink).hasClass('ideatorrent-votewidget-link-voted-highlighted'))
          currentVote = -1;
        else if($(plusnolink).hasClass('ideatorrent-votewidget-link-voted-highlighted'))
          currentVote = 1;
      }
      if($(this).attr('id') == $(pluslink).attr('id'))
        newVote = 1;
      else if($(this).attr('id') == $(minuslink).attr('id'))
        newVote = -1;
      scoreArea.text(currentScore + newVote - currentVote);


      //Do the AJAX call. Do not wait for it to return.
      $.post(Drupal.settings.basePath + "ideatorrent_vote/ajaxvote/" + nid + '/' + field_name + '/' + newVote,
        { token: tokens['#' + $(this).attr('id')] },
        function(data){}, "json");


      //Tell the arrows there is now a vote cast
      $(pluslink).add(equallink).add(minuslink).removeClass('ideatorrent-votewidget-link-unvoted');

      //Update the status of the arrows
      $(pluslink).add(equallink).add(minuslink).addClass('ideatorrent-votewidget-link-voted-unhighlighted');
      $(this).removeClass('ideatorrent-votewidget-link-voted-unhighlighted');
      $(plusnolink).add(equalnolink).add(minusnolink).removeClass('ideatorrent-votewidget-link-voted-highlighted');
      $(linkToNolink['#' + $(this).attr('id')]).addClass('ideatorrent-votewidget-link-voted-highlighted')
      return false;
    });
  });

}
