<?php
?>


<div id="ideatorrent-votewidget-<?php echo $nid ?>-<?php echo $field_name; ?>" class="ideatorrent-votewidget ideatorrent-votewidget-pmzp">

  <div class="ideatorrent-votewidget-votesum-display">
    <div id="ideatorrent-votewidget-votesum-<?php echo $nid ?>-<?php echo $field_name; ?>" class="ideatorrent-votewidget-votesum">
      <?php echo $votesum; ?>
    </div>
    <div class="ideatorrent-votewidget-votesum-label">
      <?php echo t('votes'); ?>
    </div>
  </div>

  <div class="ideatorrent-votewidget-votearea">
    <div class="ideatorrent-votewidget-votearea-plus">
      <?php echo $voteup_link; ?>
    </div>
    <div class="ideatorrent-votewidget-votearea-equal">
      <?php echo $voteequal_link; ?>
    </div>
    <div class="ideatorrent-votewidget-votearea-minus">
      <?php echo $votedown_link; ?>
    </div>

    <div class="clear-both"></div>
  </div>

</div>
